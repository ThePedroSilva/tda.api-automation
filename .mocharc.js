module.exports = {
  reporter: 'mochawesome',
  reporterOptions: `json=false,reportDir=report,reportFilename=api-automation-${process.env.ENV}`,
  require: 'utils/global.js',
  spec: ["integration/**/*.test.js"]
}
