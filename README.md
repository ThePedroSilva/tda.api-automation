# API Mocha Chai

**TDA API Automation with [Mocha](https://www.npmjs.com/package/mocha) and [Chai](https://www.npmjs.com/package/chai)**

- [Install and run](#instalação-e-execução)
  - [Prerequisites](#pré-requisitos)
  - [Cloning the repository](#clonando-o-repositório)
- [API tests](#testes-de-api)
  - [Running the tests](#executando-os-testes)
  - [Results](#resultado)
- [About the project](#sobre-o-projeto)
  - [Dependencies](#dependências-utilizadas)
  - [Project structure](#estrutura-de-diretórios)
  - [Environment](#ambiente)
- [Lint](#lint)

---

## Install and run

### Prerequisites

- [Git](https://git-scm.com/download/) e [Node.js](https://nodejs.org/en/download/) needs to be installed.

### Cloning the repository

Run all the commands below in your terminal

**1** - Clone the repository and access the directory created.

```sh
git clone https://murillowelsipt@bitbucket.org/murillowelsipt/tda.api-automation.git && cd tda.api-automation
```

**2** - Install the project dependencies:

```sh
npm i
```

### API tests

The tests were performed at MBaaS and NODE environments.

#### Running the tests

If you just want to run the tests, run the following command:

> The tests will be executed according to the environment and source market

dev:

```sh
npm run test:dev:uk
```

test:

```sh
npm test:test:uk
```

uat:

```sh
npm run test:uat:uk
```

preprod:

```sh
npm test:preprod:uk
```

prod:

```sh
npm test:prod:uk
```

As variáveis por ambiente estão definidos dentro dos arquivos _*.config.js_ em [config/](config).
Environment variables are defined in _*.env_ files in [envs/](envs).

#### Results

O resultado dos testes são apresentados no terminal e em report HTML gerado com [mochawesome](https://www.npmjs.com/package/mochawesome).

## About the project

### Dependencies
| lib | descrição
| --- | ---
| [Mocha](https://www.npmjs.com/package/mocha) | Structure (`describe`,`it`, etc.) and automation runner
| [Chai](https://www.npmjs.com/package/chai)| Assertion in BDD / TDD format
| [Mochawesome](https://www.npmjs.com/package/mochawesome)| Generates HTML report
| [Standard](https://www.npmjs.com/package/standard)| Linter and JS code formatter
| [Cross-env](https://www.npmjs.com/package/cross-env)| Creation of environment variable

Dependencies are defined in [package.json](./package.json).

### Project structure

```
tda.api-automation/
├── README.md
├── envs
│   ├── dev.env
│   ├── preprod.env
│   ├── prod.env
│   ├── test.env
│   └── uat.env
├── fixtures
├── integration
│   ├── healthCheck
│   │   └── GET-healthCheck.test.js
│   └── refreshToken[DE\ User\ Login]
│       ├── requests
│       │   └── GET-refreshToken.js
│       └── tests
│           └── GET-refreshToken.test.js
├── package-lock.json
├── package.json
├── report
└── utils
    └── global.js
```

### Environment

## Lint

It is good practice that the files are standardized, following the JS style pattern.
For this, the lib [Standard](https://www.npmjs.com/package/standard) is used.

After your implementation is complete, run:

`npm run lint:fix`
