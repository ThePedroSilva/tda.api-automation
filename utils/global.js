const chai = require('chai'); const chaiHttp = require('chai-http')
chai.use(chaiHttp)
require('dotenv').config({ path: `envs/${process.env.NODE_ENV}.env` })

const setSourceMarket = (sm) => {
  const nodeBaseUrl = process.env.BASE_URL_NODE

  return nodeBaseUrl.replace('SM', sm)
}

const awsBaseUrl = process.env.BASE_URL_AWS
const nodeBaseUrl = setSourceMarket(process.env.SM)

global.awsRequest = chai.request(awsBaseUrl)
global.nodeRequest = chai.request(nodeBaseUrl)
