const expect = require('chai').expect;
const request = require('../../requests/getToken');

const headers = {
  'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
  'app-name': 'tui-uk-th',
  'app-version': '12.3.0',
  'app-locale': 'en-GB',
  'x-api-key': '863c55cd15498f250b53013c182af3f95d6f49a9f6d802efa5ee791f505c9292',
  'x-access-token': '69a79ab88d7923ddc75c6e9439dfa5de30007f3864bea63d323e22aefc101bfc',
  'device-os': 'android',
  'correlation-id': '46c8dd96-05e2-44ef-92f3-22e1e974bac1-API Health Check'
};

const query = {
  id: 10388407,
  additionalInfo: 'Henry',
  departureDate: '2021-06-03'
};

const sm = process.env.SM;

describe('GET refreshToken', () => {
  (sm === 'de' ? it.skip : it)('GET RefreshToken API', async () => {
    const res = await request.refreshToken(query, headers);
    const mwToken = res.header['mw-token'];

    expect(res.status).to.eq(200);
    expect(mwToken).to.not.null;
  });
});
