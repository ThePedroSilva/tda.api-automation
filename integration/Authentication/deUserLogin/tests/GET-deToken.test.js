const expect = require('chai').expect;
const request = require('../../requests/getToken');

const headers = { 
  'Content-Type': 'application/json', 
  'app-name': 'tui-de-mt', 
  'app-version': '12.3.0', 
  'app-locale': 'de-de', 
  'x-api-key': '863c55cd15498f250b53013c182af3f95d6f49a9f6d802efa5ee791f505c9292', 
  'x-access-token': '69a79ab88d7923ddc75c6e9439dfa5de30007f3864bea63d323e22aefc101bfc', 
  'device-os': 'android', 
  'tui-app-country': 'DE', 
  'correlation-id': '7497aa00-65b2-421a-8cbb-0fc60fda47cb-User Login DE'
};

const body = JSON.stringify({
  "username": "jzbshhejbs@yahoo.com",
  "password": "testtest",
  "withBookingSummaries": false
});

const sm = process.env.SM;

describe('GET DE Token', () => {
  (sm !== 'de' ? it.skip : it)('GET RefreshToken API', async () => {
    const res = await request.deGetToken(body, headers);
    const mwToken = res.header['mw-token'];

    console.log(mwToken)
    expect(res.status).to.eq(200);
    expect(mwToken).to.not.null;
  });
});
