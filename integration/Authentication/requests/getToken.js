async function refreshToken (query, headers) {
  const response = await nodeRequest.get('/api/auth/requestToken')
    .query(query)
    .set(headers);

  return response;
};

async function deGetToken (body, headers) {
  const response = await nodeRequest.post('/api/auth/userlogin')
    .send(body)
    .set(headers);

  return response;
};

module.exports = {
  refreshToken, deGetToken
};
