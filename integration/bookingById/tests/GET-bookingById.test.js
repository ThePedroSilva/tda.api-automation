const { expect } = require("chai");
const request = require('../requests/GET-bookingById');
const getToken = require('../../Authentication/requests/getToken');

const bookingRef = 30016873

const query = {
    expand: 'all'
}

const headers = {
    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8', 
    'app-name': 'tui-uk-th', 
    'app-version': '12.9.0', 
    'app-locale': 'en-GB', 
    'tui-app-img-bucket-size': 'medium', 
    'x-api-key': '863c55cd15498f250b53013c182af3f95d6f49a9f6d802efa5ee791f505c9292', 
    'x-access-token': '69a79ab88d7923ddc75c6e9439dfa5de30007f3864bea63d323e22aefc101bfc', 
    'ETag': 'W/"1056-vgPmuxHV0jh2senFlOhFMnuvMyc"', 
    'device-os': 'ios', 
    'device-category': 'tablet', 
    'tui-app-country': 'GB', 
    'correlation-id': '3573d0ff-a369-4b2c-b887-ae106530cc7f-Booking by ID', 
    'device-id': '0'
}
//Temporary solution for the reusage of the getToken request
const headers2 = {
    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
    'app-name': 'tui-uk-th',
    'app-version': '12.3.0',
    'app-locale': 'en-GB',
    'x-api-key': '863c55cd15498f250b53013c182af3f95d6f49a9f6d802efa5ee791f505c9292',
    'x-access-token': '69a79ab88d7923ddc75c6e9439dfa5de30007f3864bea63d323e22aefc101bfc',
    'device-os': 'android',
    'correlation-id': '46c8dd96-05e2-44ef-92f3-22e1e974bac1-API Health Check'
  };
  
  const query2 = {
    id: 30016873,
    additionalInfo: 'tda',
    departureDate: '2021-04-29'
  };

let mwToken;

describe('GIVEN We do somewthing', () => {
    describe('WHEN call made', () => {
        before( async () => {
            const req = await getToken.refreshToken(query2, headers2)
            mwToken = req.header['mw-token']
        });

        it('THEN it should have a 200 status code', async () => {
            const res = await request.getBookingById(query, headers, mwToken, bookingRef);
            
            expect(res.status).to.eq(200)
        });

        it('THEN it should have a bookingReference', async () => {
            const res = await request.getBookingById(query, headers, mwToken, bookingRef);

            expect(res.body.bookings).to.haveOwnProperty('bookingRef')
        });
    });
});