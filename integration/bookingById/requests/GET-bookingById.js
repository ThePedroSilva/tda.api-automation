async function getBookingById(query, headers, mwToken, bookingRef) {
    const response = await nodeRequest.get(`/api/v3/bookings/${bookingRef}`)
        .query(query)
        .set(headers)
        .set('mw-token', mwToken)

    return response
};

module.exports = {
    getBookingById
};