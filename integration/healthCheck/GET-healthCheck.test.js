const expect = require('chai').expect

const healthCheck = '/h34lthch3ck'

describe(`${healthCheck}`, () => {
  it('GET API Health Check ', async () => {
    const { body, status } = await nodeRequest.get(healthCheck)

    expect(body.version).to.eq('12.9.13')
    expect(status).to.eq(200)
  })
})
